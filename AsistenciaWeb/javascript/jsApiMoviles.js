
var ungsLocation = [-34.533755, -58.702700];
var map;
var consulta;
var movimientosMoviles;
var movil;
var coords;

function cargarMapMoviles() {
    map = L.map('mapMovil').setView(ungsLocation, 14);
    // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Agregamos el control para seleccionar Layers al mapa
    var layersControl = L.control.layers({
        "Base": baseLayer
    });
    layersControl.addTo(map);
    map.layersControl = layersControl;

}

function initCirculation() {
    movimientosMoviles = new Race("Transito", map);
}

function storeCoordinate(xVal, yVal, zVal, array) {
    array.push({ lat: xVal, lon: yVal, state: zVal });
}

var bootstrap = function () {

    var url = Config.url;
    var urlTrucks = '/supporttrucks/';
    var urlStates = '/truckstates/';
    var urlPositions = '/positions/';

    var drawer = new Drawer();

    //Request para todos los móviles
    var requestAllSupports = function () {
        return $.ajax(url + urlTrucks);
    }

    //Request para información de un móvil en especial
    var requestSupportTruck = function (supportTruck_id) {
        return $.ajax(url + urlTrucks + supportTruck_id);
    }
    //Request para obtener todos los estados 
    var requestState = function (state_id) {
        return $.ajax(url + urlStates + state_id);
    }

    //Request para obtener las posiciones de los móviles
    var requestPosition = function (state_id) {
        return $.ajax(url + urlTrucks + state_id + urlPositions);
    }

    var responseExtract = function (attr, response) {
        return response[attr]
    }
    var extractSupportTruck = function (response) {
        return responseExtract('supportTruck', response);
    }
    var extractAllSupports = function (response) {
        return responseExtract('supportTrucks', response);
    }
    var extractState = function (response) {
        return responseExtract('state', response);
    }
    var extractPosition = function (response) {
        return responseExtract('positions', response);
    }
    var drawSupportTruck = function (supportTruck) {
        consulta = supportTruck;
        drawer.drawSupportTruckInList(supportTruck.id, supportTruck.state.id, consulta.state.description);
    }
    var resolveSupportPosition = function (positions, idMovil) {
        coords = [];
        movil = null;
        var estado;
        positions.forEach(function (elements) {
            estado = elements.state;
            storeCoordinate(elements.position.lat, elements.position.lon, elements.state, coords);
        });
        movil = new Runner(idMovil, obtieneColor(estado), coords);
        movimientosMoviles.addMovil(movil);
    }


    var resolveStateSupportTruck = function () {
        // pedimos el estado con el state_id, y retornamos el movil completo
        return requestState()
            .then(function (response) {
                supportTruck.state = response.state;
                delete supportTruck.state_id;
                return supportTruck;
            });
    }

    //RESUELVE LOS MÓVILES
    var resolveAllSupports = function (moviles) {
       
        /*
        requestSupportTruck() 			// pedimos un movil al servidor
                .then(extractSupportTruck) 		// extraemos el movil de la respuesta del servidor
                .then(resolveStateSupportTruck) // resolvemos el estado
                .then(drawSupportTruck) 		// dibujamos el movil con su estado
                .done(function () {
                    console.log("Fin.");
                });
*/
        var arrayMovil = moviles;
        console.log(moviles);
        var caux = arrayMovil.length;
        var i = 0;
        
        arrayMovil.forEach(function (elements) {
    
            var idMovil = elements.id;
            requestPosition(idMovil)
                .then(extractPosition)
                .then(truck => resolveSupportPosition(truck, idMovil))
                .done(function () {
                    i++;
                    if (caux == i) {
                        movimientosMoviles.start();
                    }
                });
        });
    }

    //Empieza a buscar la información
    initCirculation();
    requestAllSupports()  //Pido todos los moviles
        .then(extractAllSupports) //Extraigo todos los moviles
        .then(resolveAllSupports) //Resuelvo Todos los moviles
        .done(function () {
        });

};

$(bootstrap);


