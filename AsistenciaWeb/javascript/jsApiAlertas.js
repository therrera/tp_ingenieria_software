var urlSM = "https://ws.smn.gob.ar/alerts/type/AL";
var urlHeroku = "https://assistanceservices.herokuapp.com/api";
var stateDia = "/alerts/day/";

var obtenerAlertasApi = function (servicioAConectar) { //Establece la conexion con el servicio
  return $.ajax(servicioAConectar);
}

var extraerAlertas = function (response, tipo) { //Extrae alertas del serv meteorólogico
  if (tipo == 1)
    return response;
  if (tipo == 2)
    return responderA('alerts', response);
}

var responderA = function (day, response) { //Devuelve un array con alertas en el dia seleccionado del combo en html
  return response[day];
}

var mostrarAlerta = function (alertas) { //Funcion que genera la etiqueta que se mostrara en el formulario html
  var datosAlertas = "";

  if (alertas.length != 0) { //Valida que existan alertas "IMPORTANTE PARA SERVICIO METEOROLOGICO"
    alertas.forEach(function (element) { //Recorre todas las alertas encontradas

      //Generamos la etiqueta a mostrar en html
      datosAlertas += '<div class="divPrincipal">' + element.title + " (Alerta Meteorológico Nro " + element.nReport + ")</div>";
      datosAlertas += "<div>" + element.description + "</div>";
      datosAlertas += "<div>Fecha de emisión: " + element.date + " a las " + element.hour + "</div>";
      datosAlertas += "<div>Próxima actualización: " + element.update + "</div>";
      datosAlertas += "<div><b>Zonas afectadas:</b></div><div>";

      for (let j = 0; j < Object.keys(element.zones).length; j++) {//Obtiene las zonas de la alerta
        if (j < Object.keys(element.zones).length - 1)
          datosAlertas += element.zones[j] + ", ";
        else
          datosAlertas += element.zones[j] + ".";
      }

      datosAlertas += "</div>";
      datosAlertas += "</br>";
    });
  }
  //Asocia en el html correspondiente la etiqueta armada
  document.getElementById("alertas").innerHTML = datosAlertas;
}

function seleccionandoServicio() { //Lee el servicio seleccionado del combobox y genera el formulario de alerta correspondiente

  var servicioSeleccionado = document.getElementById("serv").value;
  var servicioExterno = "";

  if (servicioSeleccionado == 2 && seleccionandoDia() >= 0) {
    servicioExterno = urlHeroku + stateDia + seleccionandoDia();
  }

  if (servicioSeleccionado == 1) {
    servicioExterno = urlSM;
  }

  obtenerAlertasApi(servicioExterno)
    .then(request => extraerAlertas(request, servicioSeleccionado))
    .then(mostrarAlerta)
    .done(function () {
    });
}

function seleccionandoDia() { //Devuelve el dia obtenido del valor del combox en el formulario html
  var diaSeleccionado = document.getElementById("day").value;
  return diaSeleccionado;
}

function habilitarCombo(value) { //Habilita y deshabilita el combo en base a la seleccion del servidor de alertas
  if (value == "2") {
    document.getElementById("day").disabled = false;
  } else if (value == "1") {
    document.getElementById("day").disabled = true;
    document.getElementById("day").value = "-1";
  }
}
