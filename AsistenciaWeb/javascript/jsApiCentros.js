var origen = [-34.533755, -58.702700];  // donde ce centra el mapa

function bootstrap() {

    cargarMapaCentros();

    var obj = JSON.parse(text);  //el json que esta en centroPos
    // se arma el texto que se mostrara para cada centro de asistencia
    var mostrar = "";
    var i;
    for (i = 0; i < obj.centros.length; i++) {
      mostrar += "<h5><b>Centro de asistencia N°"+obj.centros[i]._numero+" </b></br> Nombre:"+obj.centros[i]._nombre+" </br> "+"Direccion: "+obj.centros[i]._direccion+ "</br>"+ "Telefono: "+obj.centros[i]._telefono+" </br> "+ " "+obj.centros[i]._horarios+" </br></h5> ";
    }
    document.getElementById("edificios").innerHTML =mostrar;

  }

function cargarMapaCentros()   // se cargan las coordenadas del Json y luego se los ubica en el mapa
{
  var obj1 = JSON.parse(text);

  var map2 = L.map('mapidCentro').setView(origen, 14);
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map2);

 var j;
 var posx;
 var posY;
 for (j = 0; j < obj1.centros.length; j++) {
   posx = obj1.centros[j]._coordenadax;       // las coordenadas viene separadas en Latitud y longitud
   posy = obj1.centros[j]._coordenaday;
   L.marker([posx, posy]).addTo(map2).bindPopup("Centro de asistencia Nº "+obj1.centros[j]._numero).openPopup;
 }

}
