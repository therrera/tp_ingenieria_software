var guardandoPrueba;


function obtieneColor(id) {
  if (id == 0)
    return "green";
  if (id == 1)
    return "blue";
  if (id == 2)
    return "red";
}

var Race = function (name, map) {
  this.name = name;
  this.map = map;
  this.movilMoviles = [];

  this.addMovil = function (movil) {
    //Creamos el layer en el mapa para ese movil
    var movilLayer = L.layerGroup().addTo(this.map);
    // Agregamos el layer al control
    this.map.layersControl.addOverlay(movilLayer, movil.name);

    var updater = function (newPosition) {
      guardandoPrueba = newPosition;
      console.log("movil: " + movil.name + "LON" + newPosition.lon + "LAT" + newPosition.lat + " " + newPosition.state)
      var drawer = new Drawer();
      var estado = "";

      if (newPosition.state == 0)
        estado = "Disponible";
      if (newPosition.state == 1)
        estado = "En servicio";
      if (newPosition.state == 2)
        estado = "No operativo";

      //Actualiza lista de estados!!
      drawer.drawSupportTruckInList(movil.name, obtieneColor(newPosition.state), estado);
      var p = newPosition.lat;
      //console.log("new p"+newPosition.lat);


      // Opción 1.
      //    movilLayer.addLayer(L.marker(newPosition).bindPopup(movil.name));


      // Opción 2.

      movilLayer.clearLayers();
      movilLayer.addLayer(L.circleMarker(newPosition, {
        radius: 7,
        fillColor: obtieneColor(newPosition.state),
        color: "#DDD",
        weight: 1,
        opacity: 0,
        fillOpacity: 0.9
      }).bindPopup("Movil " + movil.name));

    }

    this.movilMoviles.push({
      movil: movil,
      updater: updater
    })
  }

  this.start = function () {
    this.movilMoviles.forEach(function (data) {
      var movil = data.movil;
      movil.run(data.updater);
    });
  }
};
